Source: bioperl
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Charles Plessy <plessy@debian.org>,
           Steffen Moeller <moeller@debian.org>,
           Andreas Tille <tille@debian.org>,
           Olivier Sallou <osallou@debian.org>
Section: science
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
# Unnecessary according to lintian, but building with sbuild shows the contrary.
               libmodule-build-perl
Build-Depends-Indep: perl,
                     rename,
                     libio-string-perl,
                     libdata-stag-perl,
                     libtest-most-perl,
# Recommended in Build.PL (we want them to run the tests)
                     libace-perl,
                     libalgorithm-munkres-perl,
                     libarray-compare-perl,
                     libbio-asn1-entrezgene-perl,
                     libbio-samtools-perl,
                     libclass-unload-perl,
                     libcgi-pm-perl,
                     libclone-perl,
                     libconvert-binary-c-perl,
                     libdbd-sqlite3-perl,
                     libdbd-mysql-perl,
                     libdbd-pg-perl,
                     libgd-perl,
                     libgraph-perl,
                     libgraphviz-perl,
                     libhtml-parser-perl,
                     libhtml-tableextract-perl,
                     liblist-moreutils-perl,
                     libpath-class-perl,
                     libperlio-eol-perl,
                     libpostscript-perl,
                     libset-scalar-perl,
                     libsoap-lite-perl,
                     libsort-naturally-perl,
                     libspreadsheet-parseexcel-perl,
                     libspreadsheet-writeexcel-perl,
                     libstorable-perl,
                     libsvg-perl,
                     libsvg-graph-perl,
                     libtest-memory-cycle-perl,
                     libtest-pod-perl,
                     libtest-weaken-perl,
                     liburi-perl,
                     libxml-dom-xpath-perl,
                     libxml-parser-perl,
                     libxml-sax-perl,
                     libxml-sax-writer-perl,
                     libxml-twig-perl,
                     libxml-simple-perl,
                     libxml-writer-perl,
                     libxml-libxml-perl,
                     libwww-perl,
                     libyaml-perl,
                     libtest-requiresinternet-perl,
# Needed to avoid downloading DTDs during the tests and therefore fail when network is not available:
                     libxml-sax-expatxs-perl
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/med-team/bioperl
Vcs-Git: https://salsa.debian.org/med-team/bioperl.git
Homepage: http://www.bioperl.org/
Rules-Requires-Root: no

Package: bioperl
Architecture: all
Depends: libbio-perl-perl (= ${source:Version}),
         ${misc:Depends},
         ${perl:Depends},
         libtest-most-perl
Recommends: bioperl-run,
# for the following see #650412
# most of `binary -help` outputs
            perl-doc,
# some libraries already in libbio-perl-perl's Recommends:
#             bp_biofetch_genbank_proxy
            libhttp-message-perl,
            libwww-perl,
            libcache-cache-perl,
#             bp_blast2tree
            libbio-perl-run-perl,
#             bp_bulk_load_gff
            libdbi-perl,
#             bp_chaos_plot
            libgd-gd2-perl,
#             bp_das_server
            libapache-dbi-perl,
#             bp_download_query_genbank
            liburi-perl,
#             bp_einfo
#                liburi-perl
#                libhttp-message-perl
            libxml-simple-perl,
#             bp_fast_load_gff
#                libdbi-perl
#             bp_fetch [not for local indexing]
#             bp_flanks
#                libhttp-message-perl
#             bp_genbank2gff
#                libhttp-message-perl
#                libdbi-perl
#             bp_genbank2gff3
            libyaml-perl,
#             bp_hivq
#                libhttp-message-perl
#                libxml-simple-perl
#             bp_meta_gff
#                libdbi-perl
#             bp_netinstall
#             bp_pairwise_kaks
#                libbio-perl-run-perl
#             bp_process_wormbase
            libace-perl,
#             bp_query_entrez_taxa
#             bp_remote_blast
#             bp_revtrans-motif
            liblist-moreutils-perl,
#             bp_taxid4species
            libxml-twig-perl,
#             bp_taxonomy2tree
            libset-scalar-perl
Suggests: groff-base
Description: Perl tools for computational molecular biology
 The Bioperl project is a coordinated effort to collect computational methods
 routinely used in bioinformatics into a set of standard CPAN-style,
 well-documented, and freely available Perl modules. It is well-accepted
 throughout the community and used in many high-profile projects, e.g.,
 Ensembl.
 .
 The recommended packages are needed to run some of the included
 binaries, for a detailed explanation including the specific Perl
 modules please see README.Debian.
 .
 The suggested package enhances the manual pages.

Package: libbio-perl-perl
Architecture: all
Section: perl
Depends: libio-string-perl,
         libdata-stag-perl,
         ${misc:Depends},
         ${perl:Depends}
# Temporary workaround as bioperl-run has not been split
Breaks: libbio-perl-run-perl (<< 1.7.3), roary (<<3.13)
Replaces: libbio-perl-run-perl (<< 1.7.3)
Recommends: bioperl-run | libbio-perl-run-perl,
# Recommended in Build.PL
            libace-perl,
            libalgorithm-munkres-perl,
            libarray-compare-perl,
            libbio-asn1-entrezgene-perl,
            libclone-perl,
            libconvert-binary-c-perl,
            libdbd-sqlite3-perl,
            libdbd-mysql-perl,
            libdbd-pg-perl,
            libgd-perl,
            libgraph-perl,
            libgraphviz-perl,
            libhtml-parser-perl,
            libhtml-tableextract-perl,
            liblist-moreutils-perl,
            libpostscript-perl,
            libset-scalar-perl,
            libsoap-lite-perl,
            libsort-naturally-perl,
            libspreadsheet-parseexcel-perl,
            libspreadsheet-writeexcel-perl,
            libstorable-perl,
            libsvg-perl,
            libsvg-graph-perl,
            liburi-perl,
            libxml-dom-xpath-perl,
            libxml-parser-perl,
            libxml-sax-perl,
            libxml-sax-writer-perl,
            libxml-simple-perl,
            libxml-twig-perl,
            libxml-writer-perl,
            libxml-libxml-perl,
            libwww-perl
Suggests: bioperl,
          libxml-sax-expatxs-perl
# Needed to avoid downloading DTDs.
Description: BioPerl core perl modules
 BioPerl is a toolkit of perl modules useful in building bioinformatics
 solutions in Perl. It is built in an object-oriented manner so that many
 modules depend on each other to achieve a task. The collection of modules in
 libbio-perl-perl consist of the core of the functionality of bioperl.
